def func(a):
    """Функция для проверки числа на краткость 3 и 5."""
    assert a > 0
    assert a == int(a)

    result = []
    if a % 3 == 0:
        result.append('foo')
    if a % 5 == 0:
        result.append('bar')

    return ' '.join(result) if result else str(a)

if __name__ == '__main__':
    assert func(15) == 'foo bar'
    assert func(25) == 'bar'
    assert func(24) == 'foo'
    assert func(17) == '17'