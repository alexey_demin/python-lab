def split(s, sep):
    i = 0
    while i < len(s):
        if s[i:].find(sep) != -1:
            yield s[i:i + s[i:].find(sep)]
            i = i + s[i:].find('.') + 1
        else:
            yield s[i:len(s)]
            break
