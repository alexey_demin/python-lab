def decorator(*types):
    def dec(func):
        def wrapper(*args):
            if len(types) != len(args):
                raise TypeError("Неверное количество аргументов")
            else:
                for param in args:
                    for type_name in types:
                        if type_name == type(param):
                            raise TypeError("Неверный тип аргументов")
            return func(*args)
        return wrapper
    return dec


@decorator(int,float)
def function(*args):
    return 123


print(function("123",{}))
