def decorator(func):
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        if type(result) == str:
            if result.isdigit():
                return int(result)
        return result
    return wrapper

@decorator
def function(*args):
    return "123"

print(function(1,2,3))
print(type(function(1,2,3)))